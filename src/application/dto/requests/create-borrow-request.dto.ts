import { IsDate, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class CreateBorrowRequestDto {
  @IsNotEmpty()
  @IsNumber()
  book_id: number;

  @IsNotEmpty()
  @IsNumber()
  member_id: number;

  @IsOptional()
  @IsDate()
  returned?: Date | null;

  @IsNotEmpty()
  @IsDate()
  due: Date;
}
