import { IsNotEmpty, IsString } from 'class-validator';

export class CreateMemberRequestDto {
  @IsNotEmpty()
  @IsString()
  name: string;
}
