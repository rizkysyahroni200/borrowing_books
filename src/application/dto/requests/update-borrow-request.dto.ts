import { IsDate, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class UpdateBorrowRequestDto {
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @IsOptional()
  @IsNumber()
  book_id?: number;

  @IsOptional()
  @IsNumber()
  member_id?: number;

  @IsDate()
  returned?: Date | null;

  @IsOptional()
  @IsDate()
  due?: Date;
}
