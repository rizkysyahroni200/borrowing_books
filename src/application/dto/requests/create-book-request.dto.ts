import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateBookRequestDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  author: string;

  @IsNumber()
  stock: number;
}
