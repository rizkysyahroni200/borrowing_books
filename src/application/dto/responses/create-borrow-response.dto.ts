export class CreateBorrowResponseDto {
  book_id: number;
  member_id: number;
  returned: Date | null;
  due: Date;
}
