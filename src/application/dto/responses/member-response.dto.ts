export class MemberResponseDto {
  id?: number;
  name: string;
  updated_at?: Date;
  created_at?: Date;
}
