export class BookResponseDto {
  id?: number;
  title: string;
  author: string;
  stock: number;
  updated_at?: Date;
  created_at?: Date;
}
