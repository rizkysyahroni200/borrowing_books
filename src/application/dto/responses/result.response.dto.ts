export class ResultResponseDto<T> {
  status: string;
  result: T;
}
