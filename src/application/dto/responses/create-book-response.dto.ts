export class CreateBookResponseDto {
  title: string;
  author: string;
  stock: number;
}
