export class BorrowResponseDto {
  id?: number;
  book_id: number;
  member_id: number;
  returned?: Date | null;
  due: Date;
  updated_at?: Date;
  created_at?: Date;
}
