import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import {
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CreateBorrowRequestDto } from 'src/application/dto/requests/create-borrow-request.dto';
import { BorrowResponseDto } from 'src/application/dto/responses/borrow-response.dto';
import { ResultResponseDto } from 'src/application/dto/responses/result.response.dto';
import { BorrowService } from 'src/domain/services/borrow/borrow.service';

@ApiTags('API Borrow')
@Controller('borrow')
export class BorrowController {
  constructor(private readonly borrowService: BorrowService) {}

  @Post('/create')
  @ApiOperation({ summary: 'Create new data borrow' })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        member_id: {
          type: 'integer',
          example: 1,
          description: 'this is unique value',
        },
        book_id: {
          type: 'integer',
          example: 1,
          description: 'this is unique value',
        },
      },
    },
  })
  @ApiResponse({
    status: 201,
    description: 'Data borrow has created successfully',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad request because not full the requirements',
  })
  async create(@Body() body): Promise<ResultResponseDto<BorrowResponseDto>> {
    const { member_id, book_id } = body;

    const date = new Date();
    date.setDate(date.getDate() + 7);

    const data: CreateBorrowRequestDto = {
      member_id,
      book_id,
      due: date,
    };

    const result = await this.borrowService.create(data);

    return {
      status: 'success',
      result,
    };
  }

  @Get('/')
  @ApiOperation({ summary: 'Get all data borrow from this api' })
  @ApiResponse({
    status: 200,
    description: 'Successfully get all data borrow list',
  })
  async get(): Promise<ResultResponseDto<BorrowResponseDto[]>> {
    const result = await this.borrowService.get();

    return {
      status: 'success',
      result,
    };
  }

  @Post('/returned/:id')
  @ApiOperation({ summary: 'Update returned borrow book' })
  @ApiParam({
    name: 'id',
    type: 'integer',
    description: 'unique value number',
    required: true,
  })
  @ApiResponse({
    status: 201,
    description:
      'Data borrow has updated successfully with filled the key returned',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad request because not full the requirements',
  })
  async updateReturned(@Param('id') id): Promise<ResultResponseDto<number>> {
    const current_date = new Date();

    const result = await this.borrowService.updateReturned(id, current_date);

    return {
      status: 'success',
      result,
    };
  }
}
