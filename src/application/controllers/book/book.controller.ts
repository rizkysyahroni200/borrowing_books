import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Query,
} from '@nestjs/common';
import {
  ApiBody,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CreateBookRequestDto } from 'src/application/dto/requests/create-book-request.dto';
import { BookResponseDto } from 'src/application/dto/responses/book-response.dto';
import { ResultResponseDto } from 'src/application/dto/responses/result.response.dto';
import { BookService } from 'src/domain/services/book/book.service';

@ApiTags('API Book')
@Controller('book')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Post('/create')
  @ApiOperation({ summary: 'Create new data book' })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        title: {
          type: 'string',
          example: 'The lord of rings',
          description: 'this is for data title book',
        },
        author: {
          type: 'string',
          example: 'Rizky',
          description: 'this is for data author book',
        },
        stock: {
          type: 'integer',
          example: 0,
          description: 'this is for data stock book',
        },
      },
    },
  })
  @ApiResponse({
    status: 201,
    description: 'Data book has created successfully',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad request because not full the requirements',
  })
  async create(
    @Body() data: CreateBookRequestDto,
  ): Promise<ResultResponseDto<BookResponseDto>> {
    if (Math.sign(data.stock) == -1 || data.stock == 0)
      throw new BadRequestException('Stock should exist');

    const result = await this.bookService.create(data);

    return {
      status: 'success',
      result,
    };
  }

  @Get('/')
  @ApiOperation({ summary: 'Get all data book from this api' })
  @ApiResponse({
    status: 200,
    description: 'Successfully get all data book list',
  })
  @ApiQuery({
    name: 'status',
    type: 'integer',
    description:
      'Rules: status 1 = get all data book including stock 0, status 2 = get all data except stock 0',
    required: false,
  })
  async get(@Query() query): Promise<ResultResponseDto<BookResponseDto[]>> {
    const { status = 1 } = query;
    const result = await this.bookService.get(status);

    return {
      status: 'success',
      result,
    };
  }
}
