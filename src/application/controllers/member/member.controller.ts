import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateMemberRequestDto } from 'src/application/dto/requests/create-member-request.dto';
import { MemberResponseDto } from 'src/application/dto/responses/member-response.dto';
import { ResultResponseDto } from 'src/application/dto/responses/result.response.dto';
import { MemberService } from 'src/domain/services/member/member.service';

@ApiTags('API Member')
@Controller('member')
export class MemberController {
  constructor(private readonly memberService: MemberService) {}

  @Post('/create')
  @ApiOperation({ summary: 'Create new data member' })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          example: 'Rizky',
          description: 'this is for name data',
        },
      },
    },
  })
  @ApiResponse({
    status: 201,
    description: 'Data member has created successfully',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad request because not full the requirements',
  })
  async create(
    @Body() data: CreateMemberRequestDto,
  ): Promise<ResultResponseDto<MemberResponseDto>> {
    const result = await this.memberService.create(data);

    return {
      status: 'success',
      result,
    };
  }

  @ApiOperation({ summary: 'Successfully get all data member from this api' })
  @ApiResponse({
    status: 200,
    description: 'Successfully get all data member list',
  })
  @Get('/')
  async get(): Promise<ResultResponseDto<MemberResponseDto[]>> {
    const result = await this.memberService.get();

    return {
      status: 'success',
      result,
    };
  }
}
