import { Module } from '@nestjs/common';
import { BookController } from './application/controllers/book/book.controller';
import { MemberController } from './application/controllers/member/member.controller';
import { BorrowController } from './application/controllers/borrow/borrow.controller';
import { BookService } from './domain/services/book/book.service';
import { MemberService } from './domain/services/member/member.service';
import { BorrowService } from './domain/services/borrow/borrow.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { DATABASE } from './config';
import { BookModel } from './infrastructure/models/book.model';
import { BookRepository } from './infrastructure/repositories/book.repository';
import { BorrowModel } from './infrastructure/models/borrow.model';
import { MemberModel } from './infrastructure/models/member.model';
import { MemberRepository } from './infrastructure/repositories/member.repository';
import { BorrowRepository } from './infrastructure/repositories/borrow.repository';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: DATABASE.host,
      dialectOptions: {
        decimalNumbers: true,
      },
      query: {
        raw: true,
      },
      port: Number(DATABASE.port || 3306),
      username: DATABASE.user,
      password: DATABASE.password,
      database: DATABASE.name,
      autoLoadModels: true,
      timezone: '+07:00',
      synchronize: true,
      // sync: { alter: false, force: false },
      // pool: {
      //   acquire: 3000,
      //   idle: 3000,
      //   max: parseInt(process.env.MAX_CON || '5', 10),
      //   min: parseInt(process.env.MIN_CON || '1', 10),
      // },
      logging: true,
    }),
    SequelizeModule.forFeature([BookModel, BorrowModel, MemberModel]),
  ],
  controllers: [BookController, MemberController, BorrowController],
  providers: [
    BookService,
    MemberService,
    BorrowService,
    BookRepository,
    MemberRepository,
    BorrowRepository,
  ],
})
export class AppModule {}
