import * as dotenv from 'dotenv';
const envFound = dotenv.config();
if (!envFound) {
  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export const APP_PORT = process.env.PORT || 5001;

export const DATABASE = {
  host: process.env.DB_HOST || 'localhost',
  user: process.env.DB_USER || 'root',
  name: process.env.DB_NAME || 'eigen',
  password: process.env.DB_PASSWORD || 'admin@A1',
  port: process.env.DB_PORT || '3306',
};
