import { Test, TestingModule } from '@nestjs/testing';
import { MemberService } from './member.service';
import { MemberRepository } from 'src/infrastructure/repositories/member.repository';
import { CreateMemberRequestDto } from 'src/application/dto/requests/create-member-request.dto';
import { BadRequestException } from '@nestjs/common';

describe('MemberService', () => {
  let memberService: MemberService;
  let memberRepository: MemberRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MemberService,
        {
          provide: MemberRepository,
          useValue: {
            create: jest.fn().mockResolvedValue([
              {
                id: 1,
                name: 'Rizky',
                updated_at: '2024-06-13T18:31:36.000Z',
                created_at: '2024-06-13T18:31:36.000Z',
              },
            ]),
            get: jest.fn().mockResolvedValue([
              {
                id: 1,
                name: 'Rizky',
                updated_at: '2024-06-13T18:31:36.000Z',
                created_at: '2024-06-13T18:31:36.000Z',
              },
            ]),
            getById: jest.fn().mockResolvedValue({
              id: 1,
              name: 'Rizky',
              updated_at: '2024-06-13T18:31:36.000Z',
              created_at: '2024-06-13T18:31:36.000Z',
            }),
          },
        },
      ],
    }).compile();

    memberService = module.get<MemberService>(MemberService);
    memberRepository = module.get<MemberRepository>(MemberRepository);
  });

  it('create data book successfully', async () => {
    const mockData: CreateMemberRequestDto = {
      name: 'Rizky',
    };

    const result = await memberService.create(mockData);

    expect(result).toEqual([
      {
        id: 1,
        name: 'Rizky',
        updated_at: '2024-06-13T18:31:36.000Z',
        created_at: '2024-06-13T18:31:36.000Z',
      },
    ]);
  });

  it('get all data member successfully', async () => {
    const mockGetMember = [
      {
        id: 1,
        name: 'Rizky',
        updated_at: '2024-06-13T18:31:36.000Z',
        created_at: '2024-06-13T18:31:36.000Z',
      },
    ];

    const result = await memberService.get();
    (memberRepository.get as jest.Mock).mockResolvedValue(mockGetMember);

    expect(result).toEqual(mockGetMember);
  });

  it('get by id to get detail data member successfully', async () => {
    const mockGetDetailMember = {
      id: 1,
      name: 'Rizky',
      updated_at: '2024-06-13T18:31:36.000Z',
      created_at: '2024-06-13T18:31:36.000Z',
    };

    const result = await memberService.getById(1);
    (memberRepository.getById as jest.Mock).mockResolvedValue(
      mockGetDetailMember,
    );

    expect(result).toEqual(mockGetDetailMember);
  });

  it('it should error if book get by id the id not filled', async () => {
    await expect(memberService.getById(null)).rejects.toThrow(
      BadRequestException,
    );
  });
});
