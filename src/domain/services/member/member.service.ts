import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateMemberRequestDto } from 'src/application/dto/requests/create-member-request.dto';
import { MemberResponseDto } from 'src/application/dto/responses/member-response.dto';
import { MemberEntity } from 'src/domain/entities/member.entity';
import { MemberRepository } from 'src/infrastructure/repositories/member.repository';

@Injectable()
export class MemberService {
  constructor(private readonly memberRepository: MemberRepository) {}

  async create(member: CreateMemberRequestDto): Promise<MemberResponseDto> {
    const data: Partial<MemberEntity> = {
      ...member,
    };
    const result = await this.memberRepository.create(data);

    return result;
  }

  async get(): Promise<MemberResponseDto[]> {
    const result = await this.memberRepository.get();

    return result;
  }

  async getById(id: number): Promise<MemberResponseDto> {
    if (!id) throw new BadRequestException('Id should be fill');

    const result = await this.memberRepository.getById(id);

    return result;
  }
}
