import { Test, TestingModule } from '@nestjs/testing';
import { BorrowService } from './borrow.service';
import { BorrowRepository } from 'src/infrastructure/repositories/borrow.repository';
import { BookService } from '../book/book.service';
import { MemberService } from '../member/member.service';
import { BookRepository } from 'src/infrastructure/repositories/book.repository';
import { CreateBorrowRequestDto } from 'src/application/dto/requests/create-borrow-request.dto';
import { BadRequestException } from '@nestjs/common';

const mockDueDateFn = jest.fn(() => new Date('2024-06-16T00:00:00Z'));

describe('BorrowService', () => {
  let borrowService: BorrowService;
  let borrowRepository: BorrowRepository;
  let bookService: BookService;
  let memberService: MemberService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BorrowService,
        {
          provide: BorrowRepository,
          useValue: {
            create: jest
              .fn()
              .mockImplementation(({ book_id, member_id, due }) => {
                if (book_id == 3) {
                  return {
                    id: 1,
                    book_id: 3,
                    member_id: 2,
                    due: mockDueDateFn(),
                    created_at: '2024-06-14T07:07:26.000Z',
                    updated_at: '2024-06-14T07:07:26.000Z',
                  };
                } else if (book_id == 7) {
                  throw new BadRequestException('Stock book is empty');
                }
              }),
            get: jest.fn().mockResolvedValue([
              {
                id: 1,
                book_id: 3,
                member_id: 2,
                book_title: 'The lord of rings',
                book_author: 'Bambang',
                member_name: 'Freyor',
                returned: null,
                due: mockDueDateFn(),
                created_at: '2024-06-14T07:07:26.000Z',
                updated_at: '2024-06-14T07:07:26.000Z',
              },
            ]),
            getById: jest.fn().mockImplementation((id, returned) => {
              if (id == 1) {
                return true;
              } else {
                return null;
              }
            }),
            getByMemberOrBook: jest
              .fn()
              .mockImplementation((member_id, book_id, returned) => {
                if (book_id == 16 && member_id == 2 && returned == false) {
                  return {
                    id: 1,
                    book_id,
                    member_id,
                    returned: null,
                    due: mockDueDateFn(),
                    created_at: '2024-06-14T07:07:26.000Z',
                    updated_at: '2024-06-14T07:07:26.000Z',
                  };
                } else if (
                  member_id == 80 &&
                  book_id == 0 &&
                  returned == false
                ) {
                  return [
                    {
                      id: 1,
                      book_id: 15,
                      member_id,
                      returned: null,
                      due: mockDueDateFn(),
                      created_at: '2024-06-14T07:07:26.000Z',
                      updated_at: '2024-06-14T07:07:26.000Z',
                    },
                    {
                      id: 2,
                      book_id: 30,
                      member_id,
                      returned: null,
                      due: mockDueDateFn(),
                      created_at: '2024-06-14T07:07:26.000Z',
                      updated_at: '2024-06-14T07:07:26.000Z',
                    },
                    {
                      id: 3,
                      book_id: 40,
                      member_id,
                      returned: null,
                      due: mockDueDateFn(),
                      created_at: '2024-06-14T07:07:26.000Z',
                      updated_at: '2024-06-14T07:07:26.000Z',
                    },
                  ];
                } else if (
                  book_id == 0 &&
                  member_id == 29 &&
                  returned == true
                ) {
                  const mockReturnedDateFn = jest.fn(
                    () => new Date('2100-06-16T00:00:00Z'),
                  );

                  return [
                    {
                      id: 1,
                      book_id,
                      member_id,
                      returned: mockReturnedDateFn(),
                      due: mockDueDateFn(),
                      created_at: '2024-06-14T07:07:26.000Z',
                      updated_at: '2024-06-14T07:07:26.000Z',
                    },
                  ];
                } else if (book_id == 8) {
                  throw new BadRequestException(
                    'Member cannot borrow books due to late returned books and waiting for next 3 days after you returned book',
                  );
                } else {
                  return [];
                }
              }),
            updateReturned: jest.fn().mockImplementation((id, returned) => {
              if (id == 1) {
                return true;
              } else {
                throw new BadRequestException(
                  'Data borrow according your id is doesnt exist',
                );
              }
            }),
          },
        },
        BookService,
        {
          provide: BookRepository,
          useValue: {
            getById: jest.fn().mockImplementation((id) => {
              if (id == 5) {
                return null;
              } else if (id == 2) {
                return {
                  id: 2,
                  title: 'The lord of rings',
                  author: 'Bambang',
                  stock: 0,
                  created_at: '2024-06-13T18:31:36.000Z',
                  updated_at: '2024-06-13T18:31:36.000Z',
                };
              } else {
                return {
                  id: 3,
                  title: 'The lord of rings',
                  author: 'Bambang',
                  stock: 10,
                  created_at: '2024-06-13T18:31:36.000Z',
                  updated_at: '2024-06-13T18:31:36.000Z',
                };
              }
            }),
            update: jest.fn(),
          },
        },
        {
          provide: MemberService,
          useValue: {
            getById: jest.fn().mockImplementation((id) => {
              if (id == 3) {
                return null;
              } else {
                return {
                  id: 2,
                  name: 'Freyor',
                  created_at: '2024-06-13T18:31:36.000Z',
                  updated_at: '2024-06-13T18:31:36.000Z',
                };
              }
            }),
          },
        },
      ],
    }).compile();

    borrowService = module.get<BorrowService>(BorrowService);
    borrowRepository = module.get<BorrowRepository>(BorrowRepository);
    bookService = module.get<BookService>(BookService);
    memberService = module.get<MemberService>(MemberService);
  });

  it('get all data borrow successfully', async () => {
    const mockGetBorrow = [
      {
        id: 1,
        book_id: 3,
        member_id: 2,
        book_title: 'The lord of rings',
        book_author: 'Bambang',
        member_name: 'Freyor',
        returned: null,
        due: mockDueDateFn(),
        created_at: '2024-06-14T07:07:26.000Z',
        updated_at: '2024-06-14T07:07:26.000Z',
      },
    ];

    const result = await borrowService.get();
    expect(result).toEqual(mockGetBorrow);
  });

  it('should create a borrow entry successfully', async () => {
    const mockBorrowRequest: CreateBorrowRequestDto = {
      book_id: 3,
      member_id: 2,
      due: mockDueDateFn(),
      returned: null,
    };

    const mockCreatedBorrow = {
      id: 1,
      book_id: 3,
      member_id: 2,
      due: mockDueDateFn(),
      created_at: '2024-06-14T07:07:26.000Z',
      updated_at: '2024-06-14T07:07:26.000Z',
    };

    const result = await borrowService.create(mockBorrowRequest);

    expect(result).toEqual(mockCreatedBorrow);
  });

  it('update data with returned data borrow successfully', async () => {
    const result = await borrowService.updateReturned(1, new Date());

    expect(result).toEqual(true);
  });

  it('should throw an error if book does not exist', async () => {
    await expect(borrowService.updateReturned(2, new Date())).rejects.toThrow(
      new BadRequestException('Data borrow according your id is doesnt exist'),
    );
  });

  it('should throw an error if book does not exist', async () => {
    const mockBorrowRequest: CreateBorrowRequestDto = {
      book_id: 5,
      member_id: 2,
      due: mockDueDateFn(),
    };

    await expect(borrowService.create(mockBorrowRequest)).rejects.toThrow(
      new BadRequestException('Book is not exist'),
    );
  });

  it('should throw an error if member does not exist', async () => {
    const mockBorrowRequest: CreateBorrowRequestDto = {
      book_id: 10,
      member_id: 3,
      due: mockDueDateFn(),
    };

    await expect(borrowService.create(mockBorrowRequest)).rejects.toThrow(
      new BadRequestException('Member is not exist'),
    );
  });

  it('should throw an error if book borrow are same', async () => {
    const mockBorrowRequest: CreateBorrowRequestDto = {
      book_id: 16,
      member_id: 2,
      due: mockDueDateFn(),
    };

    await expect(borrowService.create(mockBorrowRequest)).rejects.toThrow(
      new BadRequestException(
        'Book should not same with previous borrow the book',
      ),
    );
  });

  it('should throw an error if book borrow more than 2 book', async () => {
    const mockBorrowRequest: CreateBorrowRequestDto = {
      book_id: 90,
      member_id: 80,
      due: mockDueDateFn(),
    };

    await expect(borrowService.create(mockBorrowRequest)).rejects.toThrow(
      new BadRequestException('Borrowing the books maximum 2 book'),
    );
  });

  it('should throw an error if book stocks are empty', async () => {
    const mockBorrowRequest: CreateBorrowRequestDto = {
      book_id: 2,
      member_id: 2,
      due: mockDueDateFn(),
    };

    await expect(borrowService.create(mockBorrowRequest)).rejects.toThrow(
      new BadRequestException('Stock book is empty'),
    );
  });

  it('should throw an error if book returned late and waiting until 3 days to back borrow again', async () => {
    const mockBorrowRequest: CreateBorrowRequestDto = {
      book_id: 110,
      member_id: 29,
      due: mockDueDateFn(),
    };

    await expect(borrowService.create(mockBorrowRequest)).rejects.toThrow(
      new BadRequestException(
        'Member cannot borrow books due to late returned books and waiting for next 3 days after you returned book',
      ),
    );
  });
});
