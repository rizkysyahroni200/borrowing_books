import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateBorrowRequestDto } from 'src/application/dto/requests/create-borrow-request.dto';
import { BorrowEntity } from 'src/domain/entities/borrow.entity';
import { BorrowRepository } from 'src/infrastructure/repositories/borrow.repository';
import { BookService } from '../book/book.service';
import { UpdateBookRequestDto } from 'src/application/dto/requests/update-book.request.dto';
import { MemberService } from '../member/member.service';
import { BorrowResponseDto } from 'src/application/dto/responses/borrow-response.dto';

@Injectable()
export class BorrowService {
  constructor(
    private readonly bookService: BookService,
    private readonly memberService: MemberService,
    private readonly borrowRepository: BorrowRepository,
  ) {}
  async create(borrow: CreateBorrowRequestDto): Promise<BorrowResponseDto> {
    const data: Partial<BorrowEntity> = {
      book_id: borrow.book_id,
      member_id: borrow.member_id,
      returned: borrow.returned,
      due: borrow.due,
    };

    const get_book_by_bookId = await this.bookService.getById(borrow.book_id);
    if (!get_book_by_bookId) throw new BadRequestException('Book is not exist');

    const get_member_by_memberId = await this.memberService.getById(
      borrow.member_id,
    );
    if (!get_member_by_memberId)
      throw new BadRequestException('Member is not exist');

    const get_detail_borrow_book =
      await this.borrowRepository.getByMemberOrBook(
        borrow.member_id,
        borrow.book_id,
        false,
      );

    if (Object.values(get_detail_borrow_book).length > 0) {
      throw new BadRequestException(
        'Book should not same with previous borrow the book',
      );
    }

    const get_unreturned_borrow = await this.borrowRepository.getByMemberOrBook(
      borrow.member_id,
      0,
      false,
    );

    if (get_unreturned_borrow.length >= 2) {
      throw new BadRequestException('Borrowing the books maximum 2 book');
    }

    const get_all_returned_borrow =
      await this.borrowRepository.getByMemberOrBook(borrow.member_id, 0, true);

    for (const item of get_all_returned_borrow) {
      const returned_date = new Date(item.returned);
      const due_date = new Date(item.due);

      const penalty_date = new Date(item.returned);
      penalty_date.setDate(penalty_date.getDate() + 3);

      if (returned_date > due_date && penalty_date >= new Date()) {
        throw new BadRequestException(
          'Member cannot borrow books due to late returned books and waiting for next 3 days after you returned book',
        );
      }
    }

    if (get_book_by_bookId.stock <= 0)
      throw new BadRequestException('Stock book is empty');

    const data_book: UpdateBookRequestDto = {
      id: get_book_by_bookId.id,
      stock: get_book_by_bookId.stock + 1 || 0,
    };
    await this.bookService.update(data_book.id, data_book);

    const result = await this.borrowRepository.create(data);

    return result;
  }

  async updateReturned(id: number, returned: Date) {
    const get_borrow_byId = await this.borrowRepository.getById(id);

    if (!get_borrow_byId)
      throw new BadRequestException(
        'Data borrow according your id is doesnt exist',
      );

    const result = await this.borrowRepository.updateReturned(id, returned);

    return result;
  }

  async get(): Promise<BorrowResponseDto[]> {
    const result = await this.borrowRepository.get();

    return result;
  }
}
