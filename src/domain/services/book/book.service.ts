import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateBookRequestDto } from 'src/application/dto/requests/create-book-request.dto';
import { UpdateBookRequestDto } from 'src/application/dto/requests/update-book.request.dto';
import { BookResponseDto } from 'src/application/dto/responses/book-response.dto';
import { BookEntity } from 'src/domain/entities/book.entity';
import { BookRepository } from 'src/infrastructure/repositories/book.repository';

@Injectable()
export class BookService {
  constructor(private readonly bookRepository: BookRepository) {}

  async create(book: CreateBookRequestDto): Promise<BookResponseDto> {
    const data: Partial<BookEntity> = {
      title: book.title,
      author: book.author,
      stock: book.stock,
    };

    const result = await this.bookRepository.create(data);

    return result;
  }

  async get(status: number): Promise<BookResponseDto[]> {
    const result = await this.bookRepository.get(status);

    return result;
  }

  async getById(id: number): Promise<BookResponseDto> {
    if (!id) throw new BadRequestException('Id should be fill');

    const result = await this.bookRepository.getById(id);

    return result;
  }

  async update(id: number, book: UpdateBookRequestDto): Promise<number> {
    const get_book_by_id = await this.bookRepository.getById(id);

    if (!get_book_by_id) throw new BadRequestException('Book is not found');

    const data: Partial<BookEntity> = {
      id,
      ...book,
    };
    const result = await this.bookRepository.update(data);

    return result;
  }
}
