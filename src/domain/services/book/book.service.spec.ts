import { Test, TestingModule } from '@nestjs/testing';
import { BookService } from './book.service';
import { CreateBookRequestDto } from 'src/application/dto/requests/create-book-request.dto';
import { BookRepository } from 'src/infrastructure/repositories/book.repository';
import { UpdateBookRequestDto } from 'src/application/dto/requests/update-book.request.dto';
import { BadRequestException } from '@nestjs/common';

describe('BookService', () => {
  let bookService: BookService;
  let bookRepository: BookRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookService,
        {
          provide: BookRepository,
          useValue: {
            create: jest.fn().mockResolvedValue({
              id: 1,
              title: 'The lord of rings',
              author: 'Rizky Syahroni',
              stock: 1,
              updated_at: '2024-06-13T12:16:25.000Z',
              created_at: '2024-06-13T12:16:25.000Z',
            }),
            get: jest.fn().mockImplementation((status) => {
              if (status == 1) {
                return [
                  {
                    id: 1,
                    title: 'dasdasdas',
                    author: 'asd',
                    stock: 9,
                    updated_at: '2024-06-13T12:16:25.000Z',
                    created_at: '2024-06-13T12:16:25.000Z',
                  },
                  {
                    id: 2,
                    title: "Skletoon's",
                    author: 'Paul Tibbit',
                    stock: null,
                    updated_at: '2024-06-13T18:32:47.000Z',
                    created_at: '2024-06-13T18:32:47.000Z',
                  },
                ];
              } else {
                return [
                  {
                    id: 1,
                    title: 'dasdasdas',
                    author: 'asd',
                    stock: 9,
                    updated_at: '2024-06-13T12:16:25.000Z',
                    created_at: '2024-06-13T12:16:25.000Z',
                  },
                ];
              }
            }),
            getById: jest.fn().mockImplementation((id) => {
              if (id == 1) {
                return {
                  id: 1,
                  title: 'dasdasdas',
                  author: 'asd',
                  stock: 9,
                  created_at: '2024-06-15T03:47:34.517Z',
                  updated_at: '2024-06-15T03:47:34.517Z',
                };
              } else {
                return null;
              }
            }),
            update: jest.fn().mockImplementation(() => {
              return true;
            }),
          },
        },
      ],
    }).compile();

    bookService = module.get<BookService>(BookService);
    bookRepository = module.get<BookRepository>(BookRepository);
  });

  it('create data book successfully', async () => {
    const mockData: CreateBookRequestDto = {
      title: 'The lord of rings',
      author: 'Rizky',
      stock: 1,
    };
    const mockCreatedBook = {
      id: 1,
      title: 'The lord of rings',
      author: 'Rizky Syahroni',
      stock: 1,
      updated_at: '2024-06-13T12:16:25.000Z',
      created_at: '2024-06-13T12:16:25.000Z',
    };

    const result = await bookService.create(mockData);

    expect(result).toEqual(mockCreatedBook);
  });

  it('get all data book with status 1 successfully', async () => {
    const mockGetBook = [
      {
        id: 1,
        title: 'dasdasdas',
        author: 'asd',
        stock: 9,
        updated_at: '2024-06-13T12:16:25.000Z',
        created_at: '2024-06-13T12:16:25.000Z',
      },
      {
        id: 2,
        title: "Skletoon's",
        author: 'Paul Tibbit',
        stock: null,
        updated_at: '2024-06-13T18:32:47.000Z',
        created_at: '2024-06-13T18:32:47.000Z',
      },
    ];

    const result = await bookService.get(1);

    expect(result).toEqual(mockGetBook);
  });

  it('get all data book with status except 1 successfully', async () => {
    const mockGetBook = [
      {
        id: 1,
        title: 'dasdasdas',
        author: 'asd',
        stock: 9,
        updated_at: '2024-06-13T12:16:25.000Z',
        created_at: '2024-06-13T12:16:25.000Z',
      },
    ];

    const result = await bookService.get(2);

    expect(result).toEqual(mockGetBook);
  });

  it('get by id to get detail data book successfully', async () => {
    const mockGetDetailBook = {
      id: 1,
      title: 'dasdasdas',
      author: 'asd',
      stock: 9,
      updated_at: '2024-06-15T03:47:34.517Z',
      created_at: '2024-06-15T03:47:34.517Z',
    };

    const result = await bookService.getById(1);

    expect(result).toEqual(mockGetDetailBook);
  });

  it('it should error if book get by id the id not filled', async () => {
    await expect(bookService.getById(null)).rejects.toThrow(
      BadRequestException,
    );
  });

  it('update data book successfully', async () => {
    const mockData: UpdateBookRequestDto = {
      id: 1,
      title: 'The lord of rings',
      author: 'Adean',
      stock: 2,
    };

    const result = await bookService.update(mockData.id, mockData);

    expect(result).toEqual(true);
  });

  it('it should error if update book id not found the data', async () => {
    const mockData: UpdateBookRequestDto = {
      id: 2,
      title: 'The lord of rings',
      author: 'Adean',
      stock: 2,
    };

    await expect(bookService.update(mockData.id, mockData)).rejects.toThrow(
      new BadRequestException('Book is not found'),
    );
  });
});
