export class BorrowEntity {
  id: number;
  book_id: number;
  member_id: number;
  returned: Date;
  due: Date;
  created_at: Date;
  updated_at: Date;
}
