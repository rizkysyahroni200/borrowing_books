export class BookEntity {
  id: number;
  title: string;
  author: string;
  stock: number;
  created_at: Date;
  updated_at: Date;
}
