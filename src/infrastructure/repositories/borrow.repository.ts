import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/sequelize';
import { Op, Sequelize } from 'sequelize';
import { BorrowModel } from '../models/borrow.model';
import { BorrowEntity } from 'src/domain/entities/borrow.entity';

@Injectable()
export class BorrowRepository {
  constructor(
    @InjectModel(BorrowModel)
    private borrowModel: typeof BorrowModel,
    @InjectConnection() private sequelize: Sequelize,
  ) {}

  async create(data: Partial<BorrowEntity>): Promise<BorrowEntity> {
    const result = await this.borrowModel.create(data);

    return result as BorrowEntity;
  }

  async updateReturned(id: number, returned: Date): Promise<number> {
    const [result] = await this.borrowModel.update(
      { returned },
      {
        where: {
          id,
        },
        // returning: true,
      },
    );

    return result;
  }

  async getById(id: number): Promise<BorrowEntity> {
    const result = await this.borrowModel.findOne({
      where: {
        id,
      },
    });

    return result as BorrowEntity;
  }

  async get(): Promise<BorrowEntity[]> {
    const query = `
      SELECT bor.id, bor.book_id, bor.member_id, boo.title as 'book_title', boo.author as 'book_author', 
             m.name as 'member_name', bor.returned, bor.due, bor.created_at, bor.updated_at
      FROM borrows as bor
      INNER JOIN books as boo ON boo.id = bor.book_id
      INNER JOIN members as m ON m.id = bor.member_id
    `;

    const borrows = await this.sequelize.query(query, {
      model: BorrowModel,
      mapToModel: true,
    });

    return borrows as BorrowEntity[];
  }

  async getByMemberOrBook(
    member_id: number | null,
    book_id: number | null,
    returned: boolean,
  ): Promise<BorrowEntity[]> {
    const condition: any = {
      member_id: member_id ? member_id : { [Op.not]: null },
      book_id: book_id ? book_id : { [Op.not]: null },
      returned: returned ? { [Op.not]: null } : null,
    };

    const result = await this.borrowModel.findAll({
      where: condition,
    });

    return result as BorrowEntity[];
  }

  async getDetailByMemberOrBook(
    member_id: number,
    book_id: number,
    returned: boolean,
  ): Promise<BorrowEntity> {
    const condition: any = {
      member_id: member_id ? member_id : { [Op.not]: null },
      book_id: book_id ? book_id : { [Op.not]: null },
      returned: returned ? { [Op.not]: null } : null,
    };

    const result = await this.borrowModel.findOne({
      where: condition,
    });

    return result as BorrowEntity;
  }
}
