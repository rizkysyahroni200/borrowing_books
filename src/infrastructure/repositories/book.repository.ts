import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/sequelize';
import { Op, Sequelize } from 'sequelize';
import { BookModel } from '../models/book.model';
import { BookEntity } from 'src/domain/entities/book.entity';

@Injectable()
export class BookRepository {
  constructor(
    @InjectModel(BookModel)
    private bookModel: typeof BookModel,
    @InjectConnection() private sequelize: Sequelize,
  ) {}

  async create(data: Partial<BookEntity>): Promise<BookEntity> {
    const result = await this.bookModel.create(data);

    return result as BookEntity;
  }
  async get(status: number): Promise<BookEntity[]> {
    let result;

    if (status != 1) {
      result = await this.bookModel.findAll({
        where: {
          [Op.and]: [{ stock: { [Op.not]: null } }, { stock: { [Op.not]: 0 } }],
        },
      });
    }

    result = await this.bookModel.findAll();

    return result as BookEntity[];
  }

  async getById(id: number): Promise<BookEntity> {
    const result = await this.bookModel.findOne({
      where: {
        id,
      },
    });

    return result as BookEntity;
  }

  async update(book: Partial<BookEntity>): Promise<number> {
    const [result] = await this.bookModel.update(book, {
      where: { id: book.id },
      // returning: true,
    });

    return result;
  }
}
