import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize';
import { MemberModel } from '../models/member.model';
import { MemberEntity } from 'src/domain/entities/member.entity';

@Injectable()
export class MemberRepository {
  constructor(
    @InjectModel(MemberModel)
    private memberModel: typeof MemberModel,
    @InjectConnection() private sequelize: Sequelize,
  ) {}

  async create(data: Partial<MemberEntity>): Promise<MemberEntity> {
    const result = await this.memberModel.create(data);

    return result as MemberEntity;
  }

  async get(): Promise<MemberEntity[]> {
    const result = await this.memberModel.findAll();

    return result as MemberEntity[];
  }

  async getById(id: number): Promise<MemberEntity> {
    const result = await this.memberModel.findOne({
      where: {
        id,
      },
    });

    return result as MemberEntity;
  }
}
