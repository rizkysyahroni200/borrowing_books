import { Column, DataType, Index, Model, Table } from 'sequelize-typescript';

@Table({
  tableName: 'borrows',
  timestamps: false,
})
export class BorrowModel extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    type: DataType.INTEGER,
  })
  @Index({
    name: 'PRIMARY',
    using: 'BTREE',
    order: 'ASC',
    unique: true,
  })
  id?: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  book_id: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  member_id: number;

  @Column({
    type: DataType.DATE,
    allowNull: true,
    defaultValue: null,
  })
  returned: Date | null;

  @Column({
    type: DataType.DATE,
    defaultValue: null,
  })
  due: Date;

  @Column({
    type: DataType.DATE,
    defaultValue: DataType.NOW,
  })
  updated_at?: Date;

  @Column({
    type: DataType.DATE,
    defaultValue: DataType.NOW,
  })
  created_at?: Date;
}
