import { Column, DataType, Index, Model, Table } from 'sequelize-typescript';

@Table({
  tableName: 'books',
  timestamps: false,
})
export class BookModel extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    type: DataType.INTEGER,
  })
  @Index({
    name: 'PRIMARY',
    using: 'BTREE',
    order: 'ASC',
    unique: true,
  })
  id?: number;

  @Column({
    type: DataType.STRING(150),
  })
  title: string;

  @Column({
    type: DataType.STRING(150),
  })
  author: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: '0',
  })
  stock: number;

  @Column({
    type: DataType.DATE,
    defaultValue: DataType.NOW,
  })
  updated_at?: Date;

  @Column({
    type: DataType.DATE,
    defaultValue: DataType.NOW,
  })
  created_at?: Date;
}
